

/**
 *
 */
package program;
import java.util.Scanner;
/**
 * @author katsu
 *
 */
public class UAV900swap3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		Scanner stdIn = new Scanner(System.in);

		System.out.println("UAVのデータを入力せよ。");
		//----------------------------------要素設定--------------------------------------------------------------
		double capa = 39963; //電力量ws
		double cons = 67.082; //1sあたりに消費する電力w 基本 66.314+0.768(アイドリング)
		/*v=8 cons = 68.49 帰還 出発 +0.768(アイドリング)
		  v=0 cons = 65.258 ホバリング +0.768(アイドリング)*/

		double battery = 39963;
		double hobacons = 69;//ホバリング時消費電力
		double plusbattery = 55.5;//1sあたり充電される電力w

		UAV[]  myUAV = new UAV[4];
		double x = 0.0; double y = 0.0;//位置座標
		int kikan = 0;//帰還してるかしてないか
		int move = 0; int movecount =3;//動いている台数
		int hobacount =3;//ホバリングしている台数
		int sensingcount = 3; int sensingtime = 0;
		int photo = 0;//画像枚数
		int photocount = 0;//23sにつき1枚

		for(int a=0; a<myUAV.length; a++) {
		myUAV[a] = new UAV(x, y, capa, cons, battery, kikan, move, photo, photocount);
		}
		/*kikan=0 出動中
		 * kikan=1 帰還
		 * kikan=2 予備機出発
		 * */

		//UAV要素 最初40スタートだからその分減少させてスタートさせる
		myUAV[0].x =-420.0; myUAV[0].y =44.0; myUAV[0].battery =15000;
		myUAV[1].x =-435.0; myUAV[1].y = 69.0; myUAV[1].battery = 25000;
		myUAV[2].x =-405.0; myUAV[2].y = 69.0; myUAV[2].battery = 35000;
		myUAV[3].battery = 25000;
		//myUAV[4].battery = 35000;
		//myUAV[5].battery = 15000;
		//myUAV[6].battery = 25000;
		//myUAV[7].battery = 25000;
	   // myUAV[8].battery = 25000;


		//move=1動いてる =0動いてない
		myUAV[0].move=1;
		myUAV[1].move=1;
		myUAV[2].move=1;
		//UAVホバリング時代入値
		hoba[] myHOBA = new hoba[3];
		double hobax = 0.0; double hobay = 0.0;
		myHOBA[0] = new hoba(hobax, hobay);
		myHOBA[1] = new hoba(hobax, hobay);
		myHOBA[2] = new hoba(hobax, hobay);
		myUAV[0].photo =1;
		myUAV[1].photo =1;
		myUAV[2].photo =1;

		double alltime =0;//総飛行時間
		double dx = 0; double dy = 0;//位置座標
		int ymode = 1;//y進み方
		int i = 0;//myUAVのID
		//int z = 0;//threshould判断の変数
		int hoba = 0;//ホバリング
		int thre = 0;//閾値
		int k = 0;
		int l = 0;
		int s = 0;
		int L1=0;
		int L2 = 0;
		int L1count = 0;
		int right = 0;
		int rightcount =0;
		int yusen = 0;
		int swaptime = 0;
		int swapcount = 0;
		double swapx = 0; double swapy= 0;
		double swapthould =0;
		double receivebat = 0;//受信ノードバッテリー消費量
		double trancebat = 0;//送信ノードバッテリー消費量
		int allphoto = 0;//基地局帰還したデータ枚数
		double batmin = 0;//swap時に判断する最小バッテリー量
		int batminjudge = 0;//swap時に判断する変数
		int yminjudge = 0;
		double ymin=0;

		correct[] correct = new correct[100];
		double Ctime = 0.0; int Allphoto = 0;
		for(int a=0; a<correct.length; a++) {
		correct[a] = new correct(Ctime, Allphoto);
		}
		int c = 0; //収集に使う関数
		int h = 0;//hobaの値で使う
		double rx=-1000;
		double ry=0;





		/*初期状態の各UAV*/
		System.out.println("***************************************************************************");
		for(i=0; i<myUAV.length; i++) {
			System.out.println("No" +(i+1) + "現在地(" + myUAV[i].getX() + ", " + myUAV[i].getY() + ")バッテリー" + myUAV[i].getFuel() + "画像枚数" +myUAV[i].getPhoto());
		}
		System.out.println("***************************************************************************");



		//-------------------------------------残バッテリー>MAXバッテリーのエラー処理----------------------------
		for(i=0; i<myUAV.length; i++) {
			do {
				if (!myUAV[i].over(capa, battery)) {
					System.out.println("残バッテリーがMaxバッテリーをオーバーしています！");
					System.out.print("残バッテリー: ");  // battery = stdIn.nextDouble();
				}
				else {
					myUAV[i].setBattery(myUAV[i].battery);
					break;
				}
			}while(true);

		}
		//-------------------------------------------シミュレーション開始 ----------------------------------------
		loop1:while (true) {
			System.out.println("終了[0...Yes] 飛行[1...Yes]：");
			int inputNumber  = stdIn.nextInt();

			if (inputNumber == 0) {
				for(c=0; c<correct.length; c++) {
				System.out.println("回収した時間:" + correct[c].Ctime + ",枚数:" +correct[c].Allphoto);
				}
				break;
			}


			 if (inputNumber == 1) {

				System.out.println("何秒飛行する？ : ");
				double j=0;

				double dtime = stdIn.nextDouble();

				while(j< dtime) {//時間
					L1= -1;
					L2= -1;
					//--------------------------------------------移動---------------------------------------------------
					for(i=0; i<myUAV.length/*移動UAV台数*/; i++) {
						if(myUAV[i].getX() == 435 && myUAV[i].getY() == 937) {
							System.out.println("終了***************************************************************************");

							for(c=0; c<correct.length; c++) {
								System.out.println("回収した時間:" + correct[c].Ctime + ",枚数:" +correct[c].Allphoto);
								}
								return;
						}else if(myUAV[i].getY() == 40 || myUAV[i].getY() == 937 || myUAV[i].getY() == 65){//右移動
							right = 1;
						}
						else if(ymode == 1 && myUAV[i].getY() != 937) {//上向き
							dx = 0;
						    dy = 4;
						    //System.out.println("上向き" +ymode);
						}else if(ymode == -1 && myUAV[i].getY() != 40){//下向き
							dx = 0;
							dy = -4;
							//System.out.println("下向き");
						}
					}if(right == 1) {
						if(hoba == 1) {

						}else if(rightcount == 14) {
								dx = 4;
								dy = 0;
								rightcount += 1;
								for(i=0; i<myUAV.length/*移動UAV台数*/; i++) {
									if(myUAV[i].getY() == 40) {
										yusen = 1;
									}
								}
							}else if(rightcount == 15) {
								if(ymode==1) {//下向き
									dx = 0;
									dy = -4;
									ymode = ymode*(-1);
									right = 0;
									rightcount = 0;
								}else {//上向き
									dx = 0;
								    dy = 4;
									ymode = ymode*(-1);
									right = 0;
									rightcount = 0;
									yusen = 0;
									System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaa1");
								}
							}else {
							dx = 4;
							dy = 0;
							rightcount += 1;
							//System.out.println("right:"+right);
							}
							System.out.println("rightcount:"+rightcount);
					}
					if(hoba==1) {

						System.out.println("---------------------------------------------");
						System.out.println("飛行時間:" +(alltime+j+1)+ "s, sensingtime:" +(sensingtime+1)+ "s" );
					}else {

					System.out.println("---------------------------------------------");
					System.out.println("飛行時間:" +(alltime+j+1)+ "s, sensingtime:" +(sensingtime+1)+ "s");
					}
					for(i=0; i<myUAV.length/*移動UAV台数*/; i++) {


						if(myUAV[i].move==1) {//動いてる

							if(myUAV[i].kikan == 2) {//-------------------------------------------予備機出発-----------------------------------------------


								double gotime = Math.sqrt(myHOBA[l].hobagetX()*myHOBA[l].hobagetX() +  myHOBA[l].hobagetY()*myHOBA[l].hobagetY())/8;//位置まで行く時間

								double dddx= myHOBA[l].hobagetX();
								double	dddy= myHOBA[l].hobagetY();

								System.out.println("dddx" +dddx+ "dddy" +dddy+ "gotime" +gotime);

									if (myUAV[i].go(1, dddx, dddy, gotime)== 0) {
										System.out.println("燃料が足りません！");
										break loop1;
									}else if(L1>=0 && myUAV[i].getY() >= myHOBA[l].hobagetY()){
										//System.out.println("check1");
										myUAV[i].x=myHOBA[l].hobagetX();
										myUAV[i].y=myHOBA[l].hobagetY();
										// 元の位置に戻る
										System.out.println("No" + (i+1) + "現在地(" + myUAV[i].getX() + ", " + myUAV[i].getY() + "),残バッテリー " + (String.format("%.2f", myUAV[i].getFuel())));
										L2=l;
										System.out.println("L2:" +l);
										/*for(L=l; L<2; L++) {
											myHOBA[L].hobax=myHOBA[L+1].hobagetX();
											myHOBA[L].hobay=myHOBA[L+1].hobagetY();
										}
										myHOBA[2].hobax=0;
										myHOBA[2].hobay=0;*/
										sensingcount += 1;
										myUAV[i].kikan = 0;
										hobacount += 1;
										System.out.println("hovacount" +hobacount);
										k -= 1;


									}else if( myUAV[i].getY() >= myHOBA[l].hobagetY() || Math.sqrt(myUAV[i].getX()*myUAV[i].getX() +  myUAV[i].getY()*myUAV[i].getY())/8 > gotime) {
										//System.out.println("check1");
										myUAV[i].x=myHOBA[l].hobagetX();
										myUAV[i].y=myHOBA[l].hobagetY();
										// 元の位置に戻る
										System.out.println("No" + (i+1) + "現在地(" + myUAV[i].getX() + ", " + myUAV[i].getY() + "),残バッテリー " + (String.format("%.2f", myUAV[i].getFuel())));
										L1=l;
										System.out.println("L1:" +l);
										sensingcount += 1;
										myUAV[i].kikan = 0;
										hobacount += 1;
										System.out.println("hovacount" +hobacount);
										k -= 1;
										L1count += 1;

									}else {//移動中
										//System.out.println("check2");
										System.out.println("No" + (i+1) + "現在地(" + (String.format("%.2f", myUAV[i].getX())) + ", " + (String.format("%.2f", myUAV[i].getY())) + "),残バッテリー " + (String.format("%.2f", myUAV[i].getFuel())));


									}
									l+=1;

							}else if(myUAV[i].kikan == 1) {//--------------------------------------------帰還----------------------------------------------
								double ddx=myUAV[i].getX();
								double	ddy=myUAV[i].getY();
								double cometime = Math.sqrt( myUAV[i].getX()* myUAV[i].getX() +  myUAV[i].getY()* myUAV[i].getY())/8;  // 帰還時間=移動距離/秒速8m

								//System.out.println("allphoto" +allphoto);
									if (myUAV[i].come(1, ddx, ddy, cometime) == 0) {//燃料不足エラー
										System.out.println("燃料が足りません！");
										break loop1;
									}else if( myUAV[i].getY()<= 0.0){//帰還完了
										myUAV[i].x=0.0;
										myUAV[i].y=0.0;
										// (0,0)に帰還
										System.out.println("No" + (i+1) + "現在地(" + myUAV[i].getX() + ", " + myUAV[i].getY() + "),残バッテリー " + (String.format("%.2f", myUAV[i].getFuel())));
										movecount -= 1;
										allphoto += myUAV[i].photo;
										myUAV[i].photo = 0;

										System.out.println("飛行時間:" +(alltime+j+1)+ ",allphoto" +allphoto);
										correct[c].Ctime = (alltime+j+1);
										correct[c].Allphoto = allphoto;
										c += 1;

										System.out.println("稼働台数" +movecount);
										myUAV[i].move =0;

									}else {//帰還中
										System.out.println("→→→→→→→→→No" + (i+1) + "現在地(" + (String.format("%.2f", myUAV[i].getX())) + ", " + (String.format("%.2f", myUAV[i].getY())) + "),残バッテリー " + (String.format("%.2f", myUAV[i].getFuel())) +"画像枚数" +myUAV[i].getPhoto());
									}
							}else if(hoba == 1) {//---------------------------------ホバリング中----------------------------------------------------------
									double ddx=0;
									double	ddy=0;
									double cometime = Math.sqrt( myUAV[i].getX()* myUAV[i].getX() +  myUAV[i].getY()* myUAV[i].getY())/8;  // 帰還時間=移動距離/秒速8m

									if(myUAV[i].hobaling(1, ddx, ddy) == 0) {
										System.out.println("燃料が足りません！");
									}else {
										System.out.println("No" + (i+1) + "現在地(" + myUAV[i].getX() + ", " + myUAV[i].getY() + "),残バッテリー " + (String.format("%.2f", myUAV[i].getFuel())));
										//double yusenthould = 30000;
										double threshould = cometime*1.1*hobacons;//時間*最低燃費
										/*if(yusen == 1) {//優先エリア内からの帰還
											System.out.println("閾値" +yusenthould);
											if(myUAV[i].getFuel()<yusenthould) {
												System.out.println("次回優先エリア到着までバッテリーがもちません,(0.0,0.0)に帰還します");
												sensingcount -= 1;
												hobacount -=1;
												//System.out.println("No" +(i+1)+"hobax" +myUAV[i].getX()+ "hobay" +myUAV[i].getY());
												System.out.println("k==============================" +k);
												myHOBA[k].hobax=myUAV[i].getX();
												myHOBA[k].hobay=myUAV[i].getY();
												//System.out.println("k:" +k+ "hobax" +myHOBA[k].hobagetX()+ "hobay" +myHOBA[k].hobagetY());
												System.out.println("k:0hobax" +myHOBA[0].hobagetX()+ "hobay" +myHOBA[0].hobagetY());
												System.out.println("k:1hobax" +myHOBA[1].hobagetX()+ "hobay" +myHOBA[1].hobagetY());
												System.out.println("k:2hobax" +myHOBA[2].hobagetX()+ "hobay" +myHOBA[2].hobagetY());
													k += 1;
												//System.out.println("k=" +k);
												myUAV[i].kikan = 1;
												thre = 1;
											}
										}*/

										if(myUAV[i].getFuel()<threshould) {//limit閾値による帰還
										System.out.println("thre 活動限界に達しました,(0.0,0.0)に帰還します");
										//System.out.println("No" +(i+1)+"hobax" +myUAV[i].getX()+ "hobay" +myUAV[i].getY());
										sensingcount -= 1;
										hobacount -=1;
										myHOBA[k].hobax=myUAV[i].getX();
										myHOBA[k].hobay=myUAV[i].getY();
										System.out.println("k:0hobax" +myHOBA[0].hobagetX()+ "hobay" +myHOBA[0].hobagetY());
										System.out.println("k:1hobax" +myHOBA[1].hobagetX()+ "hobay" +myHOBA[1].hobagetY());
										System.out.println("k:2hobax" +myHOBA[2].hobagetX()+ "hobay" +myHOBA[2].hobagetY());


											k += 1;

										myUAV[i].kikan = 1;

										}
									}



							}else if(myUAV[i].fly(1, dx, dy) == 0) {
										System.out.println("燃料が足りません！");
							}else{//-------------------------------------------------観測中----------------------------------------------------------------
								myUAV[i].photocount += 1;
								if(myUAV[i].photocount == 23) {
								myUAV[i].photo += 1;
								myUAV[i].photocount =0;
								}
								//System.out.println("photocount" +myUAV[i].photocount);
								System.out.println("No" + (i+1) + "現在地(" + myUAV[i].getX() + ", " + myUAV[i].getY() + "),残バッテリー " + (String.format("%.2f", myUAV[i].getFuel()))+ "画像枚数" +myUAV[i].getPhoto());
								double cometime = Math.sqrt( myUAV[i].getX()* myUAV[i].getX() +  myUAV[i].getY()* myUAV[i].getY())/8;  // 帰還時間=移動距離/秒速8m
								System.out.println(cometime);

								//double yusenthould = 30000;
								double threshould = (cometime+1)*69.258;//時間*最低燃費
								if(dy == 4) {//上向きで進むときは8s後に帰還させるために4m/s*8s=32mプラスしてる
									double /*future*/fcometime=Math.sqrt( myUAV[i].getX()* myUAV[i].getX() +  (myUAV[i].getY()+28)* (myUAV[i].getY()+28))/8;
									swapthould = ((2+fcometime)*69.258)+554.064;//帰還時間*最低燃費+swap時間(8s)*最低燃費
									threshould = (cometime) * 69.258;
									System.out.println("A");
								}else if(dy == -4) {
									double /*future*/fcometime=Math.sqrt( myUAV[i].getX()* myUAV[i].getX() +  (myUAV[i].getY()-28)* (myUAV[i].getY()-28) )/8;
									swapthould = ((1+fcometime)*69.258)+554.064;//帰還時間*最低燃費+swap時間*最低燃費
									threshould = cometime * 69.258;
									System.out.println("B" + (myUAV[i].getY()-28));


								}else if(rightcount <8){
									double /*future*/fcometime=Math.sqrt( (myUAV[i].getX()+28)* (myUAV[i].getX()+28) +  (myUAV[i].getY())* (myUAV[i].getY()))/8;
									swapthould = (fcometime*69.258)+554.064;//帰還時間*最低燃費+swap時間*最低燃費
									threshould = (cometime) * 69.258;
									System.out.println("C");
								}else if(rightcount>=8 && ymode == -1){//途中で上向きになる
									int fcount =16;
									fcount -= rightcount;
									System.out.println(fcount);
									System.out.println("D");
									double /*future*/fcometime=Math.sqrt( (myUAV[i].getX()+fcount)*( myUAV[i].getX()+fcount) +  (myUAV[i].getY()+4*(8-fcount))* (myUAV[i].getY()+4*(8-fcount)) )/8;
									swapthould = ((2+fcometime)*69.258)+554.064;//帰還時間*最低燃費+swap時間*最低燃費
									threshould = (cometime) * 69.258;
								}else if(rightcount>=8 && ymode == 1){//途中で下向きになる
									int fcount =16;
									fcount -= rightcount;
									System.out.println(fcount);
									System.out.println("E");
									double /*future*/fcometime=Math.sqrt( (myUAV[i].getX()+fcount)*( myUAV[i].getX()+fcount) +  (myUAV[i].getY()-4*(8-fcount))* (myUAV[i].getY()-4*(8-fcount)) )/8;
									swapthould = (fcometime*69.258)+554.064;//帰還時間*最低燃費+swap時間*最低燃費
									threshould = (cometime) * 69.258;
								}

									/*if(yusen == 1) {//優先エリア内からの帰還
										//System.out.println("閾値yusen" +yusenthould);
										if(myUAV[i].getFuel()<yusenthould) {
											System.out.println("次回優先エリア到着までバッテリーがもちません,(0.0,0.0)に帰還します");
											sensingcount -= 1;
											hobacount -=1;
											//System.out.println("No" +(i+1)+"hobax" +myUAV[i].getX()+ "hobay" +myUAV[i].getY());
											System.out.println("k==============================" +k);
											myHOBA[k].hobax=myUAV[i].getX();
											myHOBA[k].hobay=myUAV[i].getY();
											//System.out.println("k:" +k+ "hobax" +myHOBA[k].hobagetX()+ "hobay" +myHOBA[k].hobagetY());
											System.out.println("k:0hobax" +myHOBA[0].hobagetX()+ "hobay" +myHOBA[0].hobagetY());
											System.out.println("k:1hobax" +myHOBA[1].hobagetX()+ "hobay" +myHOBA[1].hobagetY());
											System.out.println("k:2hobax" +myHOBA[2].hobagetX()+ "hobay" +myHOBA[2].hobagetY());
												k += 1;
											//System.out.println("k=" +k);
											myUAV[i].kikan = 1;
											thre = 1;
										}
									}*/
								//System.out.println("閾値swap" +swapthould);
								//System.out.println("閾値limit" +threshould);


								if(myUAV[i].getFuel()<swapthould) {//swapによる閾値
										System.out.println("swap準備カウント" +swapcount);

										swaptime=1;

									}/*else if(myUAV[i].getFuel()<threshould) {
										//limitの閾値による帰還

										System.out.println("活動限界に達しました,(0.0,0.0)に帰還します");
										sensingcount -= 1;
										hobacount -=1;
										swaptime = 0;
										swapcount = 0;
										//System.out.println("No" +(i+1)+"hobax" +myUAV[i].getX()+ "hobay" +myUAV[i].getY());
										System.out.println("k==============================" +k);
										myHOBA[k].hobax=myUAV[i].getX();
										myHOBA[k].hobay=myUAV[i].getY();
										//System.out.println("k:" +k+ "hobax" +myHOBA[k].hobagetX()+ "hobay" +myHOBA[k].hobagetY());
										System.out.println("k:0hobax" +myHOBA[0].hobagetX()+ "hobay" +myHOBA[0].hobagetY());
										System.out.println("k:1hobax" +myHOBA[1].hobagetX()+ "hobay" +myHOBA[1].hobagetY());
										System.out.println("k:2hobax" +myHOBA[2].hobagetX()+ "hobay" +myHOBA[2].hobagetY());
											k += 1;
										myUAV[i].kikan = 1;
										thre = 1;
									}*/

									if(myUAV[i].getFuel()<swapthould && swapcount == 7) {//limitの閾値による帰還

										System.out.println("活動限界に達しました,(0.0,0.0)に帰還します");
										sensingcount -= 1;
										hobacount -=1;
										swaptime = 0;
										//swapcount = 0;
										//System.out.println("No" +(i+1)+"hobax" +myUAV[i].getX()+ "hobay" +myUAV[i].getY());
										System.out.println("k==============================" +k);
										myHOBA[k].hobax=myUAV[i].getX();
										myHOBA[k].hobay=myUAV[i].getY();
										//System.out.println("k:" +k+ "hobax" +myHOBA[k].hobagetX()+ "hobay" +myHOBA[k].hobagetY());
										System.out.println("k:0hobax" +myHOBA[0].hobagetX()+ "hobay" +myHOBA[0].hobagetY());
										System.out.println("k:1hobax" +myHOBA[1].hobagetX()+ "hobay" +myHOBA[1].hobagetY());
										System.out.println("k:2hobax" +myHOBA[2].hobagetX()+ "hobay" +myHOBA[2].hobagetY());
											k += 1;
										myUAV[i].kikan = 1;
										thre = 1;
									}
							}

						}else {//---------------------------------------------------充電---------------------------------------------
							myUAV[i].battery += plusbattery;
							if(myUAV[i].battery >= capa) {
								myUAV[i].battery = capa;
								System.out.println("No" + (i+1) +"充電が完了:");
							}else {
							System.out.println("No" + (i+1) + "充電中...残バッテリー " + (String.format("%.2f", myUAV[i].getFuel())));
							}
						}
					}

							if(swapcount ==6) {//--------------------------------------swap判断--------------------------------------------
								int sss=0;
								int swap =0;
								System.out.println("swapします");

								for(i=0; i<myUAV.length/*移動UAV台数*/; i++) {
									if(batminjudge ==1 && myUAV[i].kikan == 0 && batmin>myUAV[i].battery) {
										batmin = myUAV[i].battery;

									}else if(batminjudge < 1 && myUAV[i].kikan == 0) {
										batmin = myUAV[i].battery;
										batminjudge += 1;
									}
								}

								for(i=0; i<myUAV.length/*移動UAV台数*/; i++) {
									if(yminjudge ==1 && myUAV[i].kikan == 0 && myUAV[i].move == 1&& ymin>myUAV[i].y) {
										ymin = myUAV[i].y;

									}else if(yminjudge < 1 && myUAV[i].kikan == 0 && myUAV[i].move == 1) {
										ymin = myUAV[i].y;
										yminjudge += 1;
									}
								}
								yminjudge = 0;
								batminjudge = 0;
								System.out.println("ymin" +ymin);
								System.out.println("batmin" +batmin);

								for(i=0; i<myUAV.length/*移動UAV台数*/; i++) {
									for(s=0; s<myUAV.length; s++) {
										System.out.println(myUAV[s].kikan+ ","+myUAV[i].y +"i>s"+myUAV[s].y +"cccc"+ myUAV[i].battery+ "=" +  batmin);
										if(swap == 1 || sss==1) {//素通り
											break;
										}else if(myUAV[s].kikan == 0 && myUAV[s].move == 1 && hobacount == 2 && myUAV[i].battery == batmin) {
											System.out.println("送信、受信におけるバッテリーを消費しますppppai");


											int data= 0;//画像枚数移動
											int photochange = 0;
												for(int d=0; d<myUAV.length; d++) {
													if(myUAV[d].kikan == 0 && myUAV[d].move == 1) {


													receivebat = 0.83*(myUAV[d].photo*0.61)/0.3;//受信電力w*{(画像枚数*転送時間s)/伝送効率%}
													trancebat =  1*(myUAV[d].photo*0.61)/0.3;//受信電力w*{(画像枚数*転送時間s)/伝送効率%}
														if(photochange == 2) {
															break;
														}else if(d==i) {//受信ノード

															System.out.print("receive["+(String.format("%.2f", receivebat)));
															myUAV[d].battery -= receivebat;
															System.out.println("]No" +(d+1) + "現在地(" + myUAV[d].getX() + ", " + myUAV[d].getY() + ")バッテリー" + (String.format("%.2f", myUAV[d].getFuel())) + "画像枚数" +myUAV[d].getPhoto());
															photochange +=1;
														}else if(myUAV[d].y>40) {//送信ノード
															System.out.print("trance["+(String.format("%.2f", trancebat)));
															myUAV[d].battery -= trancebat;
															data += myUAV[d].photo;
															myUAV[d].photo = 0;
															System.out.println("]No" +(d+1) + "現在地(" + myUAV[d].getX() + ", " + myUAV[d].getY() + ")バッテリー" +(String.format("%.2f",  myUAV[d].getFuel())) + "画像枚数" +myUAV[d].getPhoto());
															photochange +=1;
														}
													}
												}
											myUAV[i].photo += data;
											swap +=1;
										}else if(myUAV[s].kikan == 0 && myUAV[s].move == 1 && myUAV[i].y>myUAV[s].y && myUAV[i].battery == batmin){
											System.out.println("現在の座標(" +myUAV[i].x+","+myUAV[i].y+ ")変換座標(" +myUAV[s].x+","+myUAV[s].y +s+ "番目");
											swapx = myUAV[i].x;
											swapy = myUAV[i].y;
											myUAV[i].x=myUAV[s].x;
											myUAV[i].y=myUAV[s].y;
											myUAV[s].x=swapx;
											myUAV[s].y=swapy;
											System.out.println("========swap finish=======");
											//System.out.println("0" +myUAV[i].y+ "hennka" +myUAV[s].y);
											swap +=1;
											System.out.println("送信、受信におけるバッテリーを消費しますaaaaaaaa");
											int data= 0;//画像枚数移動
												for(int d=0; d<myUAV.length; d++) {

													receivebat = 0.83*(myUAV[d].photo*0.61)/0.3;//受信電力w*{(画像枚数*転送時間s)/伝送効率%}
													trancebat =  1*(myUAV[d].photo*0.61)/0.3;//受信電力w*{(画像枚数*転送時間s)/伝送効率%}
													if(d==i) {//受信ノード

														System.out.print("[receive"+(String.format("%.2f", receivebat)));
														myUAV[d].battery -= receivebat;
														System.out.println("]No" +(d+1) + "現在地(" + myUAV[d].getX() + ", " + myUAV[d].getY() + ")バッテリー" + (String.format("%.2f", myUAV[d].getFuel())) + "画像枚数" +myUAV[d].getPhoto());

													}else if(myUAV[d].y>40) {//送信ノード
														System.out.print("[trance"+(String.format("%.2f", trancebat)));
														myUAV[d].battery -= trancebat;
														data += myUAV[d].photo;
														myUAV[d].photo = 0;
														System.out.println("]No" +(d+1) + "現在地(" + myUAV[d].getX() + ", " + myUAV[d].getY() + ")バッテリー" +(String.format("%.2f",  myUAV[d].getFuel())) + "画像枚数" +myUAV[d].getPhoto());
													}
												}
											myUAV[i].photo += data;
											sss=1;
										}else if(myUAV[s].kikan == 0 && myUAV[s].move == 1 && myUAV[i].y==ymin && myUAV[i].battery == batmin && myUAV[s].battery == batmin ) {

											swap +=1;
											System.out.println("送信、受信におけるバッテリーを消費しますbbbbbbbb");
											int data= 0;//画像枚数移動
												for(int d=0; d<myUAV.length; d++) {

													receivebat = 0.83*(myUAV[d].photo*0.61)/0.3;//受信電力w*{(画像枚数*転送時間s)/伝送効率%}
													trancebat =  1*(myUAV[d].photo*0.61)/0.3;//受信電力w*{(画像枚数*転送時間s)/伝送効率%}
													if(d==i) {//受信ノード

														System.out.print("[receive"+(String.format("%.2f", receivebat)));
														myUAV[d].battery -= receivebat;
														System.out.println("]No" +(d+1) + "現在地(" + myUAV[d].getX() + ", " + myUAV[d].getY() + ")バッテリー" + (String.format("%.2f", myUAV[d].getFuel())) + "画像枚数" +myUAV[d].getPhoto());

													}else if(myUAV[d].y>40) {//送信ノード
														System.out.print("[trance"+(String.format("%.2f", trancebat)));
														myUAV[d].battery -= trancebat;
														data += myUAV[d].photo;
														myUAV[d].photo = 0;
														System.out.println("]No" +(d+1) + "現在地(" + myUAV[d].getX() + ", " + myUAV[d].getY() + ")バッテリー" +(String.format("%.2f",  myUAV[d].getFuel())) + "画像枚数" +myUAV[d].getPhoto());
													}
												}
											myUAV[i].photo += data;
										}
									}
								}
								System.out.println("============================");


							}
							batmin = 0;

							for(i = 0; i<myHOBA.length;i++) {//----------------------帰還するUAVの位置把握-------------------------
								if(myHOBA[i].hobay>30 && hobacount == 2 && hoba == 0) {
									for(h=0;h<myUAV.length;h++) {
										if(myUAV[h].kikan == 0 && myUAV[h].move == 1) {
											//System.out.println(myUAV[h].x+ "," +rx);
											if(myUAV[h].x>rx) {
												rx = myUAV[h].x;
												ry = myUAV[h].y;
												//System.out.println(myUAV[h].x+ "A" +rx);
											}else {
										//rx = myUAV[h].x;
										//ry = myUAV[h].y;
										//System.out.println(myUAV[h].x+ "B" +rx);

											}
										}
										//System.out.println(h+"aaaaaaaaaaaaaaaaaaaaaaaaaarx" +rx+ "bbbbbbbbbbbbbbry" +ry);
									}
									if(right == 1) {
										myHOBA[i].hobax = rx-11;
										myHOBA[i].hobay = ry-25;
									}else if(ymode == -1) {
									myHOBA[i].hobax = rx-15;
									myHOBA[i].hobay = ry-29;
									}else if(ymode == 1) {
										myHOBA[i].hobax = rx-15;
										myHOBA[i].hobay = ry-21;
									}
									System.out.println("k" +i+ "hobax" +myHOBA[i].hobagetX()+ "hobay" +myHOBA[i].hobagetY());
								}
							}
					if(movecount < 3) {//---------------------------------------------予備機選択-----------------------------------------
						for(i=0; i<myUAV.length/*移動UAV台数*/; i++) {
							if(myUAV[i].battery==capa) {
								System.out.println("予備機にNo" +(i+1)+ "を選択");
								myUAV[i].move = 1;
								myUAV[i].kikan = 2;
								movecount +=1;
								System.out.println("稼働台数" +movecount);
								if(movecount == 3) {
									break;
								}
							}
						}

					}
					if(hobacount == 3) {
						cons = 66.314;
						hoba = 0;
						thre =0;
						rx = -1000;
						ry = 0;

					}
					if(hobacount < 2/*thre ==1*/ ) {
						hoba = 1;
					}

					if(L1 >= 0) {
						System.out.println("L1:" +L1);
						for(; L1<2; L1++) {
							myHOBA[L1].hobax=myHOBA[L1+1].hobagetX();
							myHOBA[L1].hobay=myHOBA[L1+1].hobagetY();

						}
						myHOBA[2].hobax=0;
						myHOBA[2].hobay=0;
						System.out.println("L=" +0+ "x:" +myHOBA[0].hobax);
						System.out.println("L=" +1+ "x:" +myHOBA[1].hobax);
						System.out.println("L=" +2+ "x:" +myHOBA[2].hobax);
						L1 = -1;

					}

					if(L2 >= 0) {
						System.out.println("L2:" +L2);
						L2= L2-1;
						for(; L2<2; L2++) {
							myHOBA[L2].hobax=myHOBA[L2+1].hobagetX();
							myHOBA[L2].hobay=myHOBA[L2+1].hobagetY();

						}
						myHOBA[2].hobax=0;
						myHOBA[2].hobay=0;
						System.out.println("L=" +0+ "x:" +myHOBA[0].hobax);
						System.out.println("L=" +1+ "x:" +myHOBA[1].hobax);
						System.out.println("L=" +2+ "x:" +myHOBA[2].hobax);
						L2 = -1;

					}
					if(sensingcount > 1) {
						sensingtime += 1;
					}
					if(swapcount == 7) {
						swapcount =0;
					}

					if(swaptime ==1) {
						swapcount +=1;
					}

					rx = -1000;
				j+= 1;
				l = 0;
				}
				alltime += dtime;
			}

		}
	}
}
